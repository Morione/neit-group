var gulp = require('gulp');
var minifyHtml = require("gulp-minify-html");
var minifyCss = require("gulp-minify-css");
var uglify = require("gulp-uglify");
var concat = require("gulp-concat");
var removeCode = require('gulp-remove-code');
var inject = require('gulp-inject');
var gutil = require('gulp-util');
var webp   = require('gulp-webp');
var gzip   = require('gulp-gzip');

var DIST_PATH = 'dist';


gulp.task('default', function() {
  // place code for your default task here
  minifyAndConcatJs();
});

function html(){
	return gulp.src('*.html') 
	    .pipe(removeCode({ production: true }))
	   // .pipe(minifyHtml())
	    .pipe(gulp.dest(DIST_PATH));
}

function minifyAndConcatCss(){
	return gulp.src('css/*.css') 
	    .pipe(minifyCss())
	    .pipe(concat('dist.css'))
	    .pipe(gulp.dest(DIST_PATH + '/css'));
}

function minifyAndConcatJs(){
	return gulp.src(['js/contact.form.js', 'js/customers.js', 'js/i18n.js', 'js/position.js', 'js/carrier.js',])
	    .pipe(uglify({ mangle: false })
	    	// error handling
	    	.on('error', function(err) {
				gutil.log(gutil.colors.red('[Error]'), err.toString());
				this.emit('end');
			})
		)
	    .pipe(concat('dist.js'))
	    .pipe(gulp.dest(DIST_PATH + '/js'));
}

function concatVendorJs(){
	return  gulp.src([
		'zendor/jquery/jquery.min.js',
        'zendor/i18next/i18next.min.js',
        'zendor/i18next/jquery-i18next.min.js',
        'zendor/uikit/js/uikit.min.js',
        'zendor/uikit/js/components/slideshow.min.js',
        'zendor/uikit/js/components/lightbox.min.js',
        'zendor/uikit/js/components/sticky.min.js',
        'zendor/uikit/js/components/grid.min.js',    
    //    'zendor/counter/jquery.countTo.js',
	]) 
    .pipe(concat('dist-vendor.js')) 
    .pipe(gulp.dest(DIST_PATH + '/js'));
}

function concatVendorCss(){

	return gulp.src([
		'zendor/uikit/css/uikit.min.css',
		'zendor/uikit/css/components/slideshow.min.css',
		'zendor/uikit/css/components/slidenav.min.css',
	])
    .pipe(concat('dist-vendor.css'))
    .pipe(gulp.dest(DIST_PATH + '/css'));
}

function compressImages(){

  return gulp.src(DIST_PATH + '/images/**/*.{jpg,jpeg,png}')
    .pipe(webp())
    .pipe(gulp.dest(DIST_PATH + '/images/'));
}

function gzipTextFiles(){

  return gulp.src(DIST_PATH + '/**/*.{html,xml,json,css,js}')
    .pipe(gzip())
    .pipe(gulp.dest(DIST_PATH));
}


gulp.task('index', function () {
  var target = gulp.src('index.html');
  var sources = gulp.src([DIST_PATH + '/js/*.js', DIST_PATH + '/css/*.css'], {read: false});
 
  return target.pipe(inject(sources))
    .pipe(gulp.dest(DIST_PATH));
});

gulp.task('build', function(){

	// dist project
	minifyAndConcatCss();
	minifyAndConcatJs();

  // dist vendor
  concatVendorJs();
  concatVendorCss();
    
  // index.html
	var sources = gulp.src([
		 DIST_PATH + '/js/dist-vendor.js',
		 DIST_PATH + '/js/dist.js', 
		 DIST_PATH + '/css/dist-vendor.css',
		 DIST_PATH + '/css/dist.css',
		 ], {read: false});

	gulp.src(['index.html', 'kariera.html'])
	 .pipe(removeCode({ production: true }))
	 .pipe(inject(sources, {ignorePath: DIST_PATH , relative: true}))  
   .pipe(minifyHtml())
   .pipe(gulp.dest(DIST_PATH)); 

  // copy images
  gulp.src('images/**/*')
  .pipe(gulp.dest(DIST_PATH + '/images')); 

    // copy uikit fonts
	gulp.src(['zendor/uikit/fonts/**/*','fonts/**/*'])
    .pipe(gulp.dest(DIST_PATH + '/fonts')); 

    // copy support files
	gulp.src([
		'index.php',
		'.htaccess',
		'ochrana-osobnych-udajov.pdf',
		'ochrana-osobnych-udajov-cz.pdf',
		'ochrana-osobnych-udajov-de.pdf',
		'ochrana-osobnych-udajov-en.pdf'
		])
    .pipe(gulp.dest(DIST_PATH)); 

  gzipTextFiles();

});