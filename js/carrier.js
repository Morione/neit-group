// carrier.js

(function ($) {
    'use strict';
    jQuery(document).ready(function () {

    	var position = new URLSearchParams(window.location.search);
    	var id = position.get('p');

    	if (!id && positionObject[id]){
    		id = 'tester';
    	}

    	var p = positionObject[id];

    	function handleRows(array) {
    		return array.join('<br/>');
    	}

    	$('#title').text(p.title);
    	$('#place').text(p.place);
    	$('#salary').text(p.salary);
    	$('#performance_date').text(p.performance_date);
    	$('#employment').text(p.employment);
    	$('#job_description').html(handleRows(p.job_description));
    	$('#benefits').html(handleRows(p.benefits));
		$('#required_education').html(handleRows(p.required_education));
		$('#required_skills').html(handleRows(p.required_skills));
		 
    });


})(jQuery);


