var positionObject = {

 'dwh-bi-konzultant': 
	{

		"title": "DWH BI Konzultant - junior",
		"place": "Bratislava, Praha",
		"performance_date": "Dohodou",
		"salary": "1200€ (brutto)",
		"employment": "Plný úväzok",
		"job_description": [
			"Budovanie dátových skladov (DWH)",
			"Implementácia ETL procesov na prenos a transformáciu údajov",
			"Vytváranie reportov a analýz",
			"Analýza požiadaviek, príprava funkčnej špecifikácie, vývoj ETL, testovanie a prevádzka DWH systému"
		],

		"benefits" : [
			"Práca s najnovšími technológiami v oblasti budovania dátových skladov, Business Intelligence a Data Mining",
			"Možnosť zúčastniť sa na projektoch vo veľkých spoločnostiach pôsobiacich v bankovom, TELCO alebo retail sektore",
			"Certifikácia v rôznych oblastiach DWH/BI"
		],
		"required_education": [
			"Vysokoškolské – technické, IT alebo ekonomické zameranie",
			"vhodné aj pre absolventov"
		],

		"required_skills": [
			"znalosť SQL",
			"ovládanie niektorého z ETL nástrojov",
			"analytické myslenie, aktívny prístup k práci"
		]
	}

, 'tester': 
	{

		"title": "Tester",
		"place": "Bratislava",
		"performance_date": "Dohodou",
		"salary": "1200€ (brutto)",
		"employment": "Plný úväzok",
		"job_description": [
			"Tester sa podieľa na testovaní riešení v oblasti DWH/BI. Vykonáva manuálne / automatizované testy podľa testovacích scenárov",
			"Zodpovedá za testovanie softvérových aplikácií s dôrazom na odhaľovanie chýb z hľadiska funkčnosti, použiteľnosti a bezpečnosti využívania softvéru"
		],

		"benefits" : [
			"Práca s najnovšími technológiami v oblasti budovania dátových skladov, business intelligence a big data.",
			"Možnosť zúčastniť sa na projektoch vo veľkých spoločnostiach pôsobiacich v bankovom sektore, poisťovníctve alebo utility.",
			"Certifikácia v odbornej oblasti.",
			"Firemné benefity – Životné poistenie, dovolenka naviac, Multisport karta a iné"
		],
		 "required_education": [
			"Vysokoškolské – technické, IT alebo ekonomické zameranie."
		],

		"required_skills": [
			"1 rok praktických skúseností s testovaním softvérových aplikácií",
			"Skúsenosť s akýmkoľvek testovacím nástrojom",
			"Výhodou: ISTQB certifikácia – Foundation level",
			"Základná znalosť SQL a/alebo PL/SQL",
			"Základná znalosť Oracle DB a/alebo MS SQL",
			"Dôslednosť, precíznosť",
			"Analytické myslenie",
			"Schopnosť pracovať samostatne aj v tíme",
		]

	}
};