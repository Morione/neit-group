

jQuery(document).ready(function($) {
    'use strict';

    // Get the form.
    var form = $('#contact-form');

    // Get the messages div.
    var formMessages = $('#form-messages');

    function getErrorText(){

        var default_lng = navigator.language;
        if (default_lng == undefined){
            default_lng = 'en';
        }
        var _LNG = getUrlParam('ln', default_lng);

        var errors = {
            'en':'Unchecked GDPR approval',
            'de':'Ungeprüfte DSGVO-Genehmigung',
            'sk':'Nezaškrtnutý súhlas s GDPR',
            'cs':'Nezaškrtnuté souhlas s GDPR',
        }
        return errors[_LNG];
    }

    // Set up an event listener for the contact form.
    $('form').on('submit', function(e) {
        // Stop the browser from submitting the form.
        e.preventDefault();

        if (form.find('input[name="gdpr"]:checked').length == 0){
            $(formMessages).removeClass('success');
            $(formMessages).addClass('error');
            $(formMessages).text(getErrorText());
            return;
        }
    
        // Serialize the form data.        
        var formData = {
                          "key": "neitgroup.eu",
                          "secret": "=3y*12jp2_3m8t9uyp$o68k3*s%_86%5w1(26ciu#5ac3%m=zs",
                          "template": null,
                          "email": {
                            "recipients": "michalkalman@gmail.com",
                            "subject": form.find('input[name="subject"]').val(),
                            "text": 'From: '+form.find('input[name="name"]').val() + ' [ ' + form.find('input[name="from"]').val() + '] \n\n' + form.find('textarea[name="text"]').val(),
                            "sender": "info@neitgroup.eu",
                            "params": ""
                          }
                        }

        // Submit the form using AJAX. data: JSON.stringify(formData),  contentType: 'application/json'.    dataType: 'text',
        $.ajax({
            type: 'POST',
            url: $(form).attr('action'),
            data: JSON.stringify(formData),
            crossDomain: true,
            contentType: 'application/json'
        })
        .done(function(response) {
            // Make sure that the formMessages div has the 'success' class.
            $(formMessages).removeClass('error');
            $(formMessages).addClass('success');

            console.log(response);
            // Set the message text.
            $(formMessages).text('Message has been send.');

            // Clear the form.
            $('#name, #from, #subject, #text').val('');
        })
        .fail(function(data) {
            // Make sure that the formMessages div has the 'error' class.
            $(formMessages).removeClass('success');
            $(formMessages).addClass('error');

            // Set the message text.
            if (data.responseText !== '') {
                $(formMessages).text(data.responseText);
            } else {
                $(formMessages).text('Oops! An error occured and your message could not be sent.');
            }
        });
    });
});