/**
*
* -------------------------------------------------
* Make JS translation by https://github.com/i18next
* example: https://github.com/i18next/jquery-i18next/blob/master/example/sample.html
* --------------------------------------------------
*
**/

// parse get url params
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

// empty value if get url param not exists
function getUrlParam(parameter, defaultvalue){
    var urlparameter = defaultvalue;
    if(window.location.href.indexOf(parameter) > -1){
        urlparameter = getUrlVars()[parameter];
        }
    return urlparameter;
}
var default_lng = navigator.language;
if (default_lng == undefined){
    default_lng = 'en';
}
var _LNG = getUrlParam('ln', default_lng);

//

(function ($) {
    'use strict';
    jQuery(document).ready(function () {


    function setTranslatedDocs(language){
      var files = [
        'ochrana-osobnych-udajov-en.pdf',
        'ochrana-osobnych-udajov.pdf',
        'ochrana-osobnych-udajov-cz.pdf',
        'ochrana-osobnych-udajov-de.pdf'
      ];

      var linkElement = $("#gdpr-doc");
      var linkHomeElement = $("#home_link");
      var home_url = "index.html?ln=";
      var career_div = $(".career");
      career_div.css("visibility", "visible");

      switch(language) {
        case 'en':
            linkElement.attr('href', files[0]);
            linkHomeElement.attr('href', home_url + 'en');
            career_div.css("display", "none");
            break;
        case 'sk':
            linkElement.attr('href', files[1]);
            linkHomeElement.attr('href', home_url + 'sk');
            break;
        case 'cs':
            linkElement.attr('href', files[2]);
            linkHomeElement.attr('href', home_url + 'cs');
            break;
        case 'de':
            linkElement.attr('href', files[3]);
            linkHomeElement.attr('href', home_url + 'de');
            career_div.css("display", "none");
            break;
        default:
            linkElement.attr('href', files[0]);
      }

    }

    String.prototype.replaceAll = function(search, replacement) {
      var target = this;
      return target.split(search).join(replacement);
    };

    function setGoogleAnalytics(language){

      var code = "<script async src=\"https://www.googletagmanager.com/gtag/js?id=UA-128836479-XXX\"></script><script>window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-128836479-XXX');</script>";

      var ga = $("#google_analytics");
       switch(language) {
        case 'en':
          code = code.replaceAll("XXX", "4");
          ga.replaceWith(code);
          break;
        case 'sk':
          code = code.replaceAll("XXX", "1");
          ga.replaceWith(code);
          break;
        case 'cs':
          code = code.replaceAll("XXX", "2");
          ga.replaceWith(code);
          break;
        case 'de':
          code = code.replaceAll("XXX", "3");
          ga.replaceWith(code);
          break;
        default:
          code = code.replaceAll("XXX", "1");
          ga.replaceWith(code);
      }
    }

    setTranslatedDocs(_LNG);
    setGoogleAnalytics(_LNG);

		//translations

		i18next.init({
        lng: _LNG,
        resources: { 
          
          en: {
            translation: {
              nav: {
                home: 'HOME',
                about: 'ABOUT US',
                services: 'SERVICES',
                technologies: 'TECHNOLOGIES',
                customers: 'CUSTOMERS',
                career: 'CAREER',
                contact: 'CONTACT'
              },
              slider: {
                title: 'neit group',
                description: 'International partner for',
                description2: 'Business Intelligence and Data Warehouse'
              },
              about: {
                title: 'ABOUT US',
                description: 'Close cooperation between MAIND from Slovakia and Neit Consulting from the Czech Republic has culminated in the creation of the joint NEIT GROUP business group. The group covers both companies on the Slovak and Czech markets in business areas, especially Data Warehouse and Business Intelligence.',
                description2: 'We strive to provide professional service and expert consulting to data processing customers in finance, retail, hotels, gastronomy, cableways, attractions and warehousing.',
                image_div: 'Learn more about us'
              },
              foto: {
                desc1: 'Chairman of the Board',
                desc2: 'Member of the Board'
              },
              services: {
                  title: 'SERVICES',
                  I: 'Business Intelligence,',
                  Ib: 'Data Analytics',
                  I_desc: 'Monitoring of real-time performance, analysis and rapid assessment of data and information, elegant reporting.',
                  II: 'Datawarehouse,',
                  IIb: 'Operational Data Store',
                  II_desc: 'We help you collect, extract and save all your data. We accelerate your application processes using ODS.',
                  III: '',
                  IIIb: 'Big Data',
                  III_desc: 'We help you identify and analyse all available data. We process and turn them into relevant and intelligible information.',
                  IV: 'Master Data Management,',
                  IVb: 'Data Solutions',
                  IV_desc: 'We find the optimum processing solution for your data. We select the right analytical tools. We recommend a suitable form of presentation.',
                  V: 'Data Integration,',
                  Vb: 'Data Quality',
                  V_desc: 'We parse your data and combine them into logical elements. We find connections. We select what’s most important from large quantities of data.',
                  VI: 'Planning,',
                  VIb: 'Financial Consolidation',
                  VI_desc: 'We’ll set up a custom budgeting and planning system for you. We automate your data consolidation. We save time, allowing you to focus on what’s most important.',
                  VII: 'Business Process',
                  VIIb: ' Management',
                  VII_desc: 'We help you improve all your processes. We automate routine activities. We improve your performance and your bottom line.',
                  VIII: '',
                  VIIIb: 'Application Development',
                  VIII_desc: 'We craft custom applications. We offer development services using various user devices, platforms and programming languages.',
                  IX: '',
                  IXb: 'Complex Testing',
                  IX_desc: 'We know of many different types of tests. We can supply you with testers or provide you with a comprehensive testing solution. ',
                },
              technologies: {
                title: 'TECHNOLOGIES'
              },
              customers: {
                title: 'CUSTOMERS'
              },
              career: {
                title: 'Aktuálna ponuka voľných pracovných miest',
                subtitle: 'Kariera',
              },
              contact: {
                title: 'CONTACT FORM',
                company_a: 'Representation in Slovakia:',
                company_b: 'Representation in Czech Republic:',
                name: 'Name',
                email: 'Email',
                subject: 'Company',
                message: 'Message',
                send: 'Send',
                gdpr: 'Consent to processing of ',
                gdpr2: 'personal data',
              }
            }
          },
          // ------ sk ---------------------------
          sk: {
            translation: {
              nav: {
                home: 'DOMOV',
                about: 'O NÁS',
                services: 'SLUŽBY',
                technologies: 'TECHNOLÓGIE',
                customers: 'ZÁKAZNÍCI',
                career: 'KARIÉRA',
                contact: 'KONTAKT'
              },    
              slider: {
                title: 'neit group',
                description: 'Medzinárodný partner v oblasti',
                description2: 'Business Intelligence a Data Warehouse'
              },
              about: {
                  title: 'O NÁS',
                  description: 'Úzka spolupráca slovenskej spoločnosti MAIND a českej Neit Consulting vyvrcholila vytvorením spoločnej obchodno-dodávateľskej skupiny NEIT GROUP. Tá zastrešuje obe spoločnosti na slovenskom a českom trhu v oblastiach Data Warehouse, Business Inteligence a BPM.',
                  description2: 'Pre zákazníkov zabezpečujeme služby a odborné poradenstvo pri spracovaní a prezentácii dát vo všetkých sektoroch trhu.',
                  image_div: ''
              },
              foto: {
                desc1: 'Predseda predstavenstva',
                desc2: 'Člen predstavenstva'
              },
              services: {
                  title: 'SLUŽBY',
                  I: 'Business Intelligence,',
                  Ib: 'Data Analytics',
                  I_desc: 'Sledovanie reálnej výkonnosti, analýza a rýchle vyhodnotenie dát a informácií v prehľadnom reporte.',
                  II: 'Datawarehouse,',
                  IIb: 'Operational Data Store',
                  II_desc: 'Pomôžeme zhromažďovať, extrahovať a ukladať vaše dáta. Zároveň pomocou ODS zrýchlime procesy potrebné pre aplikácie.',
                  III: '',
                  IIIb: 'Big Data',
                  III_desc: 'Neštrukturalizované dáta, ktoré máte dostupné pomôžeme rozanalyzovať a identifikovať tie najdôležitejšie. Dáta spracujeme a vytvoríme relevantné informácie.',
                  IV: 'Master Data Management,',
                  IVb: 'Data Solutions',
                  IV_desc: 'Nájdeme spôsob ako spracovať vaše dáta, akú vhodnú analýzu použiť a aký výstup dát bude zodpovedať vašim požiadavkám.',
                  V: 'Data Integration,',
                  Vb: 'Data Quality',
                  V_desc: 'Z množstva získaných dát očistíme a vyberieme len tie dôležité dáta, ktoré použijeme na detailnejšiu analýzu procesov a dát.',
                  VI: 'Planning,',
                  VIb: 'Financial Consolidation',
                  VI_desc: 'Pomocou automatizácie dokážeme skonsolidovať dáta od zberu cez analýzu až po reporting. Ušetríme tak čas a aj presnosť vyhodnotenia.',
                  VII: 'Business Process',
                  VIIb: ' Management',
                  VII_desc: 'Zameriavame sa aj na metódy zlepšenia podnikových procesov a zlepšovaní výkonnosti firiem, ktoré vedú aj k vyššiemu zisku.',
                  VIII: '',
                  VIIIb: 'Application Development',
                  VIII_desc: 'Aplikácie vám vyrobíme a to naprieč používanými zariadeniami, programovacími jazykmi a platformami.',
                  IX: '',
                  IXb: 'Complex Testing',
                  IX_desc: 'Robíme viac druhov testov, pričom skúšame viaceré scenáre, rozlíšenia a analyzujeme viaceré prostredia.',
                },
              technologies: {
                title: 'TECHNOLÓGIE'
              },
              customers: {
                title: 'ZÁKAZNÍCI'
              },
              career: {
                title: 'Aktuálna ponuka voľných pracovných miest',
                subtitle: 'Kariera',
              },
              contact: {
                title: 'KONTAKT',
                company_a: 'Zastúpenie pre Slovensko:',
                company_b: 'Zastúpenie pre Českú republiku:',
                name: 'Meno',
                email: 'Email',
                subject: 'Firma',
                message: 'Správa',
                send: 'Poslať',
                gdpr: 'Súhlasím so ',
                gdpr2: 'spracovaním osobných údajov',
              }
            }
          },
          // ------ cz ---------------------------
          cs: {
            translation: {
              nav: {
                home: 'DOMŮ',
                about: 'O NÁS',
                services: 'SLUŽBY',
                technologies: 'TECHNOLOGIE',
                customers: 'ZÁKAZNÍCI',
                career: 'KARIERA',
                contact: 'KONTAKT'
              },
              slider: {
                title: 'neit group',
                description: 'Mezinárodní partner pro oblast',
                description2: 'Business Intelligence a Data Warehouse'
              },
              about: {
                title: 'O NÁS',
                description: 'Úzká spolupráce slovenské společnosti MAIND a české Neit Consulting vyvrcholila vytvořením společné obchodně dodavatelské skupiny Neit GROUP. Ta zastřešuje obě společnosti na českém a slovenském trhu v konkrétních oblastech byznysu, jakými jsou především Data Warehouse a Business Intelligence.',
                description2: 'Pro zákazníky se snažíme zajistit profesionální servis a odborné poradenství při zpracování dat z oblastí financí, prodejů, hotelů, gastronomie, lanových drah, atrakcí a skladů.',
                image_div: ''
              },
              foto: {
                desc1: 'Předseda představenstva',
                desc2: 'Člen představenstva'
              },
              services: {
                 title: 'SLUŽBY',
                  I: 'Business Intelligence,',
                  Ib: 'Data Analytics',
                  I_desc: 'Monitorování reálné výkonnosti, analýza a rychlé vyhodnocení dat a informací, elegantní reporting.',
                  II: 'Datawarehouse,',
                  IIb: 'Operational Data Store',
                  II_desc: 'Pomůžeme vám shromáždit, extrahovat a uložit všechna vaše data. S využitím ODS zrychlíme vaše aplikační procesy.',
                  III: '',
                  IIIb: 'Big Data',
                  III_desc: 'Všechna dostupná data vám pomůžeme identifikovat a analyzovat. Zpracujeme je a přeměníme na relevantní srozumitelné informace.',
                  IV: 'Master Data Management,',
                  IVb: 'Data Solutions',
                  IV_desc: 'Nájdeme spôsob ako spracovať vaše dáta, akú vhodnú analýzu použiť a aký výstup dát bude zodpovedať vašim požiadavkám.',
                  V: 'Data Integration,',
                  Vb: 'Data Quality',
                  V_desc: 'Nalezneme optimální způsob zpracování vašich dat. Vybereme vhodné analytické nástroje. Doporučíme vhodný způsob prezentace.',
                  VI: 'Planning,',
                  VIb: 'Financial Consolidation',
                  VI_desc: 'Nastavíme vám na míru systém pro zadávání rozpočtů a plánů. Zautomatizujeme vám konsolidaci dat. Ušetříme váš čas, abyste se mohli věnovat tomu důležitému.',
                  VII: 'Business Process',
                  VIIb: ' Management',
                  VII_desc: 'Pomůžeme vám zlepšit vaše procesy. Rutinní aktivity zautomatizujeme. Zlepšíme vaši výkonnost a zvýšíme váš zisk.',
                  VIII: '',
                  VIIIb: 'Application Development',
                  VIII_desc: 'Vytvoříme vám aplikaci na míru. Umíme to napříč různými uživatelskými zařízeními, platformami a programovacími jazyky.',
                  IX: '',
                  IXb: 'Complex Testing',
                  IX_desc: 'Umíme mnoho různých druhů testů. Můžeme vám dodat testery nebo za vás celou problematiku testování kompletně vyřešit.',
                },
              technologies: {
                title: 'TECHNOLOGIE'
              },
              customers: {
                title: 'ZÁKAZNÍCI'
              },
              career: {
                title: 'Aktuálna ponuka voľných pracovných miest',
                subtitle: 'Kariera',
              },
              contact: {
                title: 'KONTAKT',
                company_a: 'Zastoupení pro Slovensko:',
                company_b: 'Zastoupení pro Českou republiku:',
                name: 'Jméno',
                email: 'E-mail',
                subject: 'Společnost',
                message: 'Zpráva',
                send: 'Poslat',
                gdpr: 'Súhlasím so ',
                gdpr2: 'zpracováním osobních údajů',
              }
            }
          },
          // ------ de ---------------------------
          de: {
            translation: {
              nav: {
                home: 'HAUSE',
                about: 'ÜBER UNS',
                services: 'LEISTUNGEN',
                technologies: 'TECHNOLOGIEN',
                customers: 'KUNDEN',
                career: 'KARRIER',
                contact: 'KONTAKT'
              },
              slider: {
                title: 'neit group',
                description: 'Ihr internationaler Partner für',
                description2: 'Business Intelligence und Data Warehouse.'
              },
              about: {
                title: 'ÜBER UNS',
                description: 'Die enge Zusammenarbeit der slowakischen Firma MAIND und der tschechischen Neit Consulting fand ihren Höhepunkt in der Gründung einer gemeinsamen Handelsgruppe – der NEIT GROUP. Diese überdacht beide Unternehmen auf dem slowakischen und tschechischen Markt, insbesondere in den Bereichen Data Warehouse und Business Intelligence.',
                description2: 'Wir bieten unseren Kunden einen professionellen Service und Fachberatung bei der Datenverarbeitung in den Bereichen Finanzen, Handel, Hotels, Gastronomie, Seilbahnen, Attraktionen und Lager.',
                image_div: 'Learn more about us'
              },
              foto: {
                desc1: 'Präsident des Verwaltungsrates',
                desc2: 'Mitglied des Verwaltungsrats'
              },
              services: {
                title: 'LEISTUNGEN',
                  I: 'Business Intelligence,',
                  Ib: 'Data Analytics',
                  I_desc: 'Überwachung der realen Leistungsfähigkeit, Analyse und schnelle Auswertung von Daten und Informationen, elegantes Reporting.',
                  II: 'Datawarehouse,',
                  IIb: 'Operational Data Store',
                  II_desc: 'Wir helfen Ihnen, all Ihre Daten zu sammeln, zu extrahieren und zu speichern. Unter Nutzung von ODS beschleunigen wir Ihre Applikationsprozesse.',
                  III: '',
                  IIIb: 'Big Data',
                  III_desc: 'Wir helfen Ihnen, alle verfügbaren Daten zu identifizieren und zu analysieren. Wir verarbeiten sie und wandeln sie in verständliche Informationen um.',
                  IV: 'Master Data Management,',
                  IVb: 'Data Solutions',
                  IV_desc: 'Wir finden das optimale Verarbeitungsverfahren für Ihre Daten. Wir suchen geeignete Analysetools aus. Wir empfehlen Ihnen eine geeignete Art der Präsentation.',
                  V: 'Data Integration,',
                  Vb: 'Data Quality',
                  V_desc: 'Wir bereinigen Ihre Daten und verbinden sie zu logischen Einheiten. Wir finden Zusammenhänge. Aus einer großen Datenmenge suchen wir die wichtigsten Daten heraus.',
                  VI: 'Planning,',
                  VIb: 'Financial Consolidation',
                  VI_desc: 'Wir stellen Ihr Budget und Plansystem maßgeschneidert auf Ihre Bedürfnisse ein. Wir automatisieren Ihre Datenkonsolidierung. Wir sparen Ihnen Zeit, die Sie für wichtigere Dinge nutzen können.',
                  VII: 'Business Process',
                  VIIb: ' Management',
                  VII_desc: 'Wir helfen Ihnen, Ihre Prozesse zu verbessern. Wir automatisieren Routineaktivitäten. Wir verbessern Ihre Leistungsfähigkeit und steigern Ihren Gewinn.',
                  VIII: '',
                  VIIIb: 'Application Development',
                  VIII_desc: 'Wir erstellen für Sie maßgeschneiderte Applikationen. Quer durch diverse Anwendergeräte, Plattformen und Programmiersprachen.',
                  IX: '',
                  IXb: 'Complex Testing',
                  IX_desc: 'Wir kennen viele verschiedene Arten von Tests. Wir liefern Ihnen Tester oder übernehmen für Sie alles rund ums Testen.',
                },
              technologies: {
                title: 'TECHNOLOGIEN'
              },
              customers: {
                title: 'KUNDEN'
              },
              career: {
                title: 'Aktuálna ponuka voľných pracovných miest',
                subtitle: 'Kariera',
              },
              contact: {
                title: 'KONTAKT',
                company_a: 'Vertretung für die Slowakische Republik:',
                company_b: 'Vertretung für die Tschechische Republik:',
                name: 'Name',
                email: 'E-Mail',
                subject: 'Firma',
                message: 'Nachricht',
                send: 'Senden',
                gdpr: 'Ich stimme ',
                gdpr2: 'der Verarbeitung meiner personenbezogenen Daten zu.',
              }
            }
          }
        }
      }, function(err, t) {
        // for options see
        // https://github.com/i18next/jquery-i18next#initialize-the-plugin
        jqueryI18next.init(i18next, $);
        // start localizing, details:
        // https://github.com/i18next/jquery-i18next#usage-of-selector-function
        $('.tc-header').localize();
        $('.uk-offcanvas').localize();
        $('.tc-footer').localize();
        $('.tc-main').localize();
        $('.tc-slider').localize();
        $('.tc-services').localize();
      });

    });

    // --- language action buttons
    $('.flag').click(function() {
      var ln = $(this).data('ln');
      var url = '?ln=' + ln;
      $(location).attr('href', url);
    });

})(jQuery);

